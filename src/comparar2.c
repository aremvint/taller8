#include "funciones.h"

int comp_extrano(int a,int b){

	if(a>b)
		return 0;
	if(a<b)
		return 1;
	else
		return -1;

}

void insertionSort(int arr[],int n,ptrfn comp){

	int i, key, j;
	for(i=1;i<n;i++){

		key=arr[i];
		j=i-1;
		while(j>=0 && comp(arr[j],key)>0){
		
			arr[j+1] = arr[j];
			j = j-1;
		
		}
		arr[j+1] = key;
	}

}
