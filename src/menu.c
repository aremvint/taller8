#include <stdio.h>
#include "funciones.h"

int main()
{
	
	//array de 20 elementos
	int arr[20]={13,2,7,15,9,20,1,6,11,14,3,4,19,5,10,8,18,12,17,16};

	//int tamano determinara la cantidad de elementos
	int tamano= sizeof(arr)/sizeof(arr[0]);

	// typedef int (*ptrfn)(int, int) por lo q apunta a una funcion q recibe dos parametros y bota un int
	ptrfn puntero1=&comp1; //

	//se pasa un puntero a funcion, este es puntero1
	insertionSort(arr,tamano,puntero1);
	printf("\nFuncion COMP1\n");
	
	for(int i=0;i<tamano;i++){
		printf("%d ",arr[i]);
	}

	ptrfn puntero2=&comp2;
	insertionSort(arr,tamano,puntero2);
	printf("\nFuncion COMP2\n");
	
	for(int i=0;i<tamano;i++){
		printf("%d ",arr[i]);
	}
	
	
	ptrfn puntero3=&comp_extrano;
	insertionSort(arr,tamano,puntero3);
	printf("\nFuncion COMP_EXTRANO\n");
	
	for(int i=0;i<tamano;i++){
		printf("%d ",arr[i]);
	}
	return 0;
}
