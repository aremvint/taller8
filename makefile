# Como simplificación final, usemos las macros especiales $ @ y $ ^ , 
#que son los lados izquierdo y derecho de :
#el $ < es el primer elemento en la lista de dependencias
# -I es para que busque en el directorio actual
# La bandera -c dice que para generar el archivo objeto


bin/funciones: obj/menu.o obj/comparar.o obj/comparar2.o
	gcc -Wall $^ -o $@
obj/menu.o: src/menu.c
	gcc -Wall -Iinclude $< -c -o $@
obj/comparar.o: src/comparar.c
	gcc -Wall -Iinclude $< -c -o $@
obj/comparar2.o: src/comparar2.c
	gcc -Wall -Iinclude $< -c -o $@

run:
	./bin/funciones
clean:
	rm obj

	