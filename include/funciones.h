typedef int (*ptrfn)(int,int);
//Esto significa que las variables declaradas con el tipo ptrfn son 
//punteros a una función que toma dos parámetros int y devuelve un int

void insertionSort(int arr[],int n,ptrfn comp);

int comp1(int a, int b);
int comp2(int a, int b);
int comp_extrano(int a, int b);
